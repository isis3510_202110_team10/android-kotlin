package com.mimenu.client.Singletons

object UserData {

    init
    {
        println("Singleton class invoked.")
    }

    var idToken : String = ""
    var accessToken : String = ""
    var latitude : Double = 0.0
    var longitude : Double = 0.0
    var idVenue : String=""

    fun setIdToken(iToken:String):String {
        idToken = iToken
        return idToken
    }

    fun setAccessToken(aToken:String):String {
        accessToken = aToken
        return accessToken
    }

    fun setLatitude(lat: Double): Double{
        latitude = lat
        return latitude
    }

    fun setLongitude(lon: Double): Double{
        longitude = lon
        return longitude
    }

    fun setIdVenue(id: String):String{
        idVenue=id
        return idVenue
    }
}