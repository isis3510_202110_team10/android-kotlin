package com.mimenu.client.ViewHolders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.mimenu.client.Adapters.AdapterOrderProduct
import kotlinx.android.synthetic.main.order_product_cards.view.*

class OrderProductViewHolder (val view: View, private val listener: AdapterOrderProduct.onItemClickListener,private val parentPosition: Int) : RecyclerView.ViewHolder(view),View.OnClickListener{

    init{
        view.buttonCancel.setOnClickListener(this)
        view.buttonReject.setOnClickListener(this)
        view.buttonDeliver.setOnClickListener(this)
        view.buttonAccept.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        var description = p0?.resources!!.getResourceEntryName(p0?.id!!)
        val position : Int= adapterPosition
        if(position != RecyclerView.NO_POSITION){
            listener.onItemClick(position,parentPosition,description)
        }
    }


}