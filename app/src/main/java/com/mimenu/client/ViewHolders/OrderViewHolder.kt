package com.mimenu.client.ViewHolders

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class OrderViewHolder(val view: View) : RecyclerView.ViewHolder(view)