package com.mimenu.client.ViewHolders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.mimenu.client.Adapters.AdapterVenue
import kotlinx.android.synthetic.main.sede_cards.view.*

class VenueViewHolder (val view: View, private val listener: AdapterVenue.onItemClickListener): RecyclerView.ViewHolder(view), View.OnClickListener{

    init{
        view.buttonCoor.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        val position : Int= adapterPosition
        if(position != RecyclerView.NO_POSITION){
            listener.onItemClick(position)
        }
    }

}