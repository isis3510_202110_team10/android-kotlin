package com.mimenu.client.Data

import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.mimenu.client.Model.Venue

object ConverterJsonVenue {

    fun createVenues(data: JsonArray): ArrayList<Venue> {
        val items = ArrayList<Venue>()
        var gson = Gson()
        var items1 : JsonObject?

        for (i in 0 until  data.size()) {
            items1 = data.get(i) as JsonObject?
            var venue = gson.fromJson(items1, Venue::class.java)

                items.add(venue)


        }
        return items
    }
}