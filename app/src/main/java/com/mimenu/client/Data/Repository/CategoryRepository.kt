package com.mimenu.client.Data.Repository

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.room.withTransaction
import com.mimenu.client.API.AvailableAPI
import com.mimenu.client.Data.Converter.ConverterJsonCategory
import com.mimenu.client.Data.DataBase
import com.mimenu.client.Model.RequestAvailable
import com.mimenu.client.Services.networkBoundResource
import com.mimenu.client.Singletons.UserData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

class CategoryRepository @Inject constructor(
        private val api:AvailableAPI,
        private val db : DataBase
){
    private val categoriesDao= db.categoriesDao()

    private val requestsDao=db.requestsDao()

    fun getAvailables()=networkBoundResource(
            query={
                categoriesDao.getAllCategories()
            },
            fetch={
                delay(2000)
               val response = api.getAvailables(getHeaderMap())
                ConverterJsonCategory.createCategories(response)
            },
            saveFetchResult = {
                categories ->
                db.withTransaction{
                    categoriesDao.deleteCategories()
                    categoriesDao.insertCategories(categories)
                }
            }

    )

    @RequiresApi(Build.VERSION_CODES.M)
    fun updateAvailable(request:RequestAvailable){

        CoroutineScope(Dispatchers.IO).launch {
            val params = getParams(request)
            try{
            val response = api.updateAvailable("application/json","Bearer ".plus(UserData.idToken), params).execute()
            if(response.isSuccessful){
                val response = api.getAvailables(getHeaderMap())
                val categories= ConverterJsonCategory.createCategories(response)
                db.withTransaction{
                    categoriesDao.deleteCategories()
                    categoriesDao.insertCategories(categories)
                }
            }
            }
            catch(t: Throwable){
                Log.i("t",t.message)
                db.withTransaction{
                    requestsDao.insertRequest(request)
                }
            }
        }
    }

    suspend fun getRequestsAvailable(): List<RequestAvailable> {
        var req= emptyList<RequestAvailable>()
        db.withTransaction{
            req=requestsDao.getAllRequests()
        }
        return req
    }

    @RequiresApi(Build.VERSION_CODES.M)
    suspend fun updateRequests(){
        val requests = getRequestsAvailable()
        requests.forEach { rq ->
            updateAvailable(rq)
            db.withTransaction{
                requestsDao.deleteRequestById(rq.id)
            }
        }
    }

    private fun getParams(request: RequestAvailable): MutableMap<String, String> {
        val params = HashMap<String, String>()
        params["item_id"] = request.item_id.toString()
        params["venue_id"] = request.id_venue.toString()
        params["operation"] = request.operation.toString()
        return params
    }

    private fun getHeaderMap(): Map<String, String> {
        val headers = HashMap<String, String>()
        val accessToken = UserData.idToken
        headers["Authorization"] = "Bearer ".plus(accessToken)
        return headers
    }
}
