package com.mimenu.client.Data.Converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.mimenu.client.Model.Table

class ConverterTable {

    @TypeConverter
    fun tableToJson(value:Table?) : String?{
        if(value==null){
            return null
        }
        return Gson().toJson(value)
    }

    @TypeConverter
    fun jsonToTable(value : String): Table = Gson().fromJson(value, Table::class.java)
}