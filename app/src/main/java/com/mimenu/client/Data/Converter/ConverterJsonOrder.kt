package com.mimenu.client.Data.Converter

import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.mimenu.client.Model.Order
import com.mimenu.client.Singletons.UserData

object ConverterJsonOrder {

    fun createOrders(data: JsonArray): ArrayList<Order> {

        val orders = ArrayList<Order>()
        var gson = Gson()
        var orderI: JsonObject?

        for (i in 0 until data.size()){
            orderI = data.get(i) as JsonObject?
            var order = gson.fromJson(orderI, Order::class.java)
            if(order.venue.equals(UserData.idVenue)){
                orders.add(order)
            }
        }
        return orders
    }
}