package com.mimenu.client.Data.Converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.mimenu.client.Model.OrderProduct

class ConverterOrderProduct {
    @TypeConverter
    fun listToJson(value: List<OrderProduct>?): String? {
        if(value==null){
            return null
        }
        return Gson().toJson(value)
    }

    @TypeConverter
    fun jsonToList(value: String) = Gson().fromJson(value, Array<OrderProduct>::class.java).toList()
}