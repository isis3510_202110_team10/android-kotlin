package com.mimenu.client.Data.Converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.mimenu.client.Model.Food


class ConverterCategory {

    @TypeConverter
    fun listToJson(value: List<Food>?): String? {
        if(value==null){
            return null
        }
        return Gson().toJson(value)
    }

    @TypeConverter
    fun jsonToList(value: String) = Gson().fromJson(value, Array<Food>::class.java).toList()

}