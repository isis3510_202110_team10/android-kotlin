package com.mimenu.client.Data

import androidx.room.withTransaction
import com.mimenu.client.API.VenueAPI
import com.mimenu.client.Model.Venue
import com.mimenu.client.Services.networkBoundResource
import com.mimenu.client.Singletons.UserData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

class VenueRepository @Inject constructor(
        private val api: VenueAPI,
        private val db : DataBase
){
    private val venuesDao= db.venueDao()
    fun getVenues()= networkBoundResource(
            query={
                venuesDao.getAllVenues()
            },
            fetch={
                delay(2000)
                val response = api.getVenues(getHeaderMap())
                ConverterJsonVenue.createVenues(response)
            },
            saveFetchResult = {
                venues ->
                db.withTransaction{
                    venuesDao.deleteVenue()
                    venuesDao.insertVenues(venues)
                }
            }

    )

    suspend fun getVenue(id:String): Venue {
        var venue=Venue()
        CoroutineScope(Dispatchers.Default).launch {
            venue=venuesDao.getVenueById(id)
        }.join()
        return venue
    }

    private fun getHeaderMap(): Map<String, String> {
        val headers = HashMap<String, String>()
        val accessToken = UserData.idToken
        headers["Authorization"] = "Bearer ".plus(accessToken)
        return headers
    }

}