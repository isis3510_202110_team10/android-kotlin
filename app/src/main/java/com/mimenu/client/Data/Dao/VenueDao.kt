package com.mimenu.client.Data.Dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mimenu.client.Model.Venue
import kotlinx.coroutines.flow.Flow

@Dao
interface VenueDao {

    @Query("SELECT * FROM Venue")
    fun getAllVenues(): Flow<List<Venue>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertVenues(venues: List<Venue>)


    @Query("DELETE FROM Venue")
    suspend fun deleteVenue()

    @Query("SELECT * FROM Venue WHERE Venue.id==:id")
    suspend fun getVenueById(id: String?):Venue
}