package com.mimenu.client.Data.Repository

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.room.withTransaction
import com.mimenu.client.API.OrderAPI
import com.mimenu.client.Data.Converter.ConverterJsonOrder
import com.mimenu.client.Data.Dao.OrderDao
import com.mimenu.client.Data.DataBase
import com.mimenu.client.Model.Order
import com.mimenu.client.Model.RequestOrder
import com.mimenu.client.Services.networkBoundResource
import com.mimenu.client.Singletons.UserData
import kotlinx.coroutines.*
import javax.inject.Inject

class OrderRepository @Inject constructor(
        private val api: OrderAPI,
        private val db : DataBase
){
    private val ordersDao:OrderDao= db.ordersDao()

    private val requestsDao=db.requestsOrderDao()

    fun getOrders()= networkBoundResource(
            query={
                ordersDao.getAllOrders()
            },
            fetch={
                delay(2000)
                val response = api.getOrders(getHeaderMap())
                ConverterJsonOrder.createOrders(response)
            },
            saveFetchResult = {
                orders ->
                db.withTransaction{
                    ordersDao.deleteOrders()
                    ordersDao.insertOrders(orders)
                }
            }
    )

    // request: has the data to be updated
    // order: in case there is no internet it is used to inform the orderProductAdapters that the order is waiting to be updated
    @RequiresApi(Build.VERSION_CODES.M)
    fun updateOrder(request: RequestOrder, order:Order?){

        CoroutineScope(Dispatchers.IO).launch {
            val params = getParams(request)
            try{
                val response = api.updateOrder("application/json","Bearer ".plus(UserData.idToken), params, request.item_id!!).execute()
                if(response.isSuccessful){
                    val response = api.getOrders(getHeaderMap())
                    val orders= ConverterJsonOrder.createOrders(response)
                    db.withTransaction{
                        ordersDao.deleteOrders()
                        ordersDao.insertOrders(orders)
                    }
                }
                else if(order!=null) inCaseThereIsNoInternet(request,order)
            }
            catch(t: Throwable){
                Log.i("error",t.message)
                if(order!=null)inCaseThereIsNoInternet(request,order)
            }
        }
    }
    fun getNewOrder()
    {
        CoroutineScope(Dispatchers.IO).launch {
            val response = api.getOrders(getHeaderMap())
            val orders = ConverterJsonOrder.createOrders(response)
            db.withTransaction {
                ordersDao.deleteOrders()
                ordersDao.insertOrders(orders)
            }
        }
    }

    suspend fun inCaseThereIsNoInternet(request: RequestOrder, order: Order){
        db.withTransaction{
            requestsDao.insertRequest(request)
            ordersDao.updateOrder(order)
        }
    }

    suspend fun getRequestsOrder(): List<RequestOrder> {
        var req= emptyList<RequestOrder>()
        db.withTransaction{
            req=requestsDao.getAllRequests()
        }
        return req
    }

    @RequiresApi(Build.VERSION_CODES.M)
    suspend fun updateRequests(){
        val requests = getRequestsOrder()
        requests.forEach { rq ->
            updateOrder(rq,null)
            db.withTransaction{
                requestsDao.deleteRequestById(rq.id)
            }
        }
    }

    private fun getParams(request : RequestOrder) : MutableMap<String, String> {
        val params = HashMap<String, String>()
        params["status"] = request.status.toString()
        return params
    }

    private fun getHeaderMap(): Map<String, String> {
        val headers = HashMap<String, String>()
        val accessToken = UserData.idToken
        headers["Authorization"] = "Bearer ".plus(accessToken)
        return headers
    }

}