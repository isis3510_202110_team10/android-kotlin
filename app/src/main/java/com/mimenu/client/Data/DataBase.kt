package com.mimenu.client.Data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.mimenu.client.Data.Converter.ConverterCategory
import com.mimenu.client.Data.Converter.ConverterChoice
import com.mimenu.client.Data.Converter.ConverterOrderProduct
import com.mimenu.client.Data.Converter.ConverterTable
import com.mimenu.client.Data.Dao.*
import com.mimenu.client.Model.*

@Database(entities = [Category::class, RequestAvailable::class, RequestOrder::class, Order::class, SelectedVenue::class, Venue::class], version = 1)
@TypeConverters(value = [ConverterCategory::class, ConverterTable::class, ConverterOrderProduct::class, ConverterChoice::class])
abstract class DataBase : RoomDatabase(){

    abstract fun categoriesDao(): CategoryDao

    abstract fun requestsDao(): RequestAvailableDao

    abstract fun ordersDao(): OrderDao

    abstract fun venueDao(): VenueDao

    abstract fun requestsOrderDao(): RequestOrderDao

    abstract fun selectedVenueDao():SelectedVenueDao

}