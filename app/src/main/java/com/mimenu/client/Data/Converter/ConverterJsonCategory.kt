package com.mimenu.client.Data.Converter

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.mimenu.client.Model.Category
import com.mimenu.client.Model.Food
import com.mimenu.client.Singletons.UserData

object ConverterJsonCategory {

    fun createCategories(data: JsonArray): ArrayList<Category> {
        val categories = ArrayList<Category>()

        for (i in 0 until  data.size()) {
            val cat: JsonObject? = data.get(i) as JsonObject?
            val items = ArrayList<Food>()
            val items1 = cat?.getAsJsonArray("items")
            if (items1 != null) {
                for (j in 0 until items1.size()) {

                    val items2 = items1.get(j) as JsonObject

                    val item = items2.get("item") as JsonObject

                    val disponibilidades = item.getAsJsonArray("not_available_at")
                    var dis = true

                    val venue = UserData.idVenue
                    if (disponibilidades.size() >= 1) {
                        for (k in 0 until disponibilidades.size()) {
                            val itemk=disponibilidades.get(k).asString
                            if (itemk.equals(venue)) dis = false
                        }
                    }


                    var id = if(item.get("id")!=null) item.get("id").asString else " "
                    var name = if(item.get("name")!=null) item.get("name").asString else " "
                    var description = if(item.get("description")!=null) item.get("description").asString else " "
                    var price = if(item.get("price")!=null) item.get("price").asInt else 0
                    var image = if(!item.get("image").toString().equals("null")) item.get("image").asString else " "

                    val it = Food(id,
                            name,
                            description,
                            price,
                            dis,
                            image)

                    items.add(it)
                    }

            }
            if (cat != null) {
                categories.add(Category(cat.get("name").asString,items))
            }
        }
        return categories
    }
}