package com.mimenu.client.Data.Dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mimenu.client.Model.RequestOrder

@Dao
interface RequestOrderDao {
    @Query("SELECT * FROM RequestOrder")
    fun getAllRequests(): List<RequestOrder>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRequest(requests: RequestOrder):Long

    @Query("DELETE FROM RequestOrder")
    suspend fun deleteRequests()

    @Query("DELETE FROM RequestOrder WHERE RequestOrder.id=:id")
    suspend fun deleteRequestById(id: Int?)
}