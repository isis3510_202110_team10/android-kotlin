package com.mimenu.client.Data.Repository


import com.mimenu.client.Data.DataBase
import com.mimenu.client.Model.SelectedVenue
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class SelectedVenueRepository @Inject constructor(
        private val db : DataBase
){
    private val selectedVenueDao = db.selectedVenueDao()

    fun insertVenue(venue:SelectedVenue){
        CoroutineScope(Dispatchers.Default).launch {
            val entro = selectedVenueDao.insertSelectedVenue(venue)
        }
    }

    fun getAll(): List<SelectedVenue> {
        var venues= emptyList<SelectedVenue>()
        CoroutineScope(Dispatchers.Default).launch {
            venues = selectedVenueDao.getAllSelectedVenues()
        }
        return venues
    }

    suspend fun getPreferedVenue():String{
        var pref= ""
        CoroutineScope(Dispatchers.Default).launch {
            val venues = selectedVenueDao.getAllSelectedVenues()
            pref= venues.groupBy{it.id_venue}.mapValues { it.value.size }.maxBy { it.value }?.key.toString()

        }.join()
        return pref
    }

}
