package com.mimenu.client.Data.Dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mimenu.client.Model.SelectedVenue

@Dao
interface SelectedVenueDao {

    @Query("SELECT * FROM selectedVenue")
    fun getAllSelectedVenues(): List<SelectedVenue>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSelectedVenues(venues: List<SelectedVenue>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSelectedVenue(venues: SelectedVenue):Long

    @Query("DELETE FROM selectedVenue")
    suspend fun deleteSelectedVenue()
}
