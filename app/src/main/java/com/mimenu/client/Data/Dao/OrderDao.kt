package com.mimenu.client.Data.Dao

import androidx.room.*
import com.mimenu.client.Model.Order
import kotlinx.coroutines.flow.Flow

@Dao
interface OrderDao {
    @Query("SELECT * FROM `Order`")
    fun getAllOrders(): Flow<List<Order>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrders(orders: List<Order>)

    @Query("DELETE FROM `Order`")
    suspend fun deleteOrders()

    @Update
    suspend fun updateOrder(order: Order)
}