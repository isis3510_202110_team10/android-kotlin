package com.mimenu.client.Data.Converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.mimenu.client.Model.Choice

class ConverterChoice {

    @TypeConverter
    fun listToJson(value: List<Choice>?): String? {
        if(value==null){
            return null
        }
        return Gson().toJson(value)
    }

    @TypeConverter
    fun jsonToList(value: String) = Gson().fromJson(value, Array<Choice>::class.java).toList()
}