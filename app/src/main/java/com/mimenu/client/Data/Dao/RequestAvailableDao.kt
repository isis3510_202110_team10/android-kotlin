package com.mimenu.client.Data.Dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mimenu.client.Model.RequestAvailable

@Dao
interface RequestAvailableDao {

    @Query("SELECT * FROM RequestAvailable")
    fun getAllRequests(): List<RequestAvailable>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRequest(requests: RequestAvailable):Long

    @Query("DELETE FROM RequestAvailable")
    suspend fun deleteRequests()

    @Query("DELETE FROM RequestAvailable WHERE RequestAvailable.id=:id")
    suspend fun deleteRequestById(id: Int?)
}