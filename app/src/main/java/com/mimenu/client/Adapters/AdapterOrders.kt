package com.mimenu.client.Adapters
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mimenu.client.Model.Order
import com.mimenu.client.R
import com.mimenu.client.UI.fragments.OrderFragment
import com.mimenu.client.ViewHolders.OrderViewHolder
import kotlinx.android.synthetic.main.order_cards.view.*


class AdapterOrders(var orders: List<Order>, private val listener: AdapterOrderProduct.onItemClickListener) : RecyclerView.Adapter<OrderViewHolder>(){

    //var orders=porders
    lateinit var adapter : AdapterOrderProduct
    override fun getItemCount()=orders.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderViewHolder {
        return OrderViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.order_cards,parent,false)
        )
    }

    override fun onBindViewHolder(holder: OrderViewHolder, position: Int) {
        var ord = orders[position]
        holder.view.order_Name.text = "Pedido #" + ord.public_id.toString() + " - " + ord.table?.name
        holder.view.recyclerViewOrderProducts.layoutManager = LinearLayoutManager(OrderFragment.newInstance().context)
        adapter = AdapterOrderProduct(ord.items, listener, position)
        holder.view.recyclerViewOrderProducts.adapter = AdapterOrderProduct(ord.items, listener, position)
    }

    fun updateList(porders: List<Order>){
            orders = porders
            notifyDataSetChanged()

    }

}