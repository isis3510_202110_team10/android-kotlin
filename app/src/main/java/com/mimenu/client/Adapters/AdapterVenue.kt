package com.mimenu.client.Adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mimenu.client.Model.Venue
import com.mimenu.client.R
import kotlinx.android.synthetic.main.sede_cards.view.*
import com.mimenu.client.ViewHolders.VenueViewHolder

class AdapterVenue (val data: List<Venue>, private val listener: onItemClickListener): RecyclerView.Adapter<VenueViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): VenueViewHolder {
        return VenueViewHolder(LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.sede_cards, viewGroup, false), listener)
    }

    override fun onBindViewHolder(viewHolder: VenueViewHolder, i: Int) {
        val venue = data[i]
        viewHolder.view.Nombre.text = venue.name
        viewHolder.view.DirRef.text= venue.address
    }

    override fun getItemCount()=data.size

    interface onItemClickListener{
        fun onItemClick(position:Int)
    }

}