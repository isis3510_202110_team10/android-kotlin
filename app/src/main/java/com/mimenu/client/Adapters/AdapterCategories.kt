package com.mimenu.client.Adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mimenu.client.Data.Repository.CategoryRepository
import com.mimenu.client.Model.Category
import com.mimenu.client.R
import com.mimenu.client.UI.fragments.AvailableFragment
import com.mimenu.client.ViewHolders.Category_Available_ViewHolder
import kotlinx.android.synthetic.main.available_categories.view.*
import javax.inject.Inject


class AdapterCategories@Inject constructor(cat: List<Category>, val repository: CategoryRepository) : RecyclerView.Adapter<Category_Available_ViewHolder>() {

    private var data= cat

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): Category_Available_ViewHolder {
        return Category_Available_ViewHolder(LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.available_categories, viewGroup, false))
    }

    override fun onBindViewHolder(viewHolder: Category_Available_ViewHolder, i: Int) {
        val item = data[i]
        viewHolder.view.category_Name.text = item.name
        val foods = data[i].foods
        viewHolder.view.recyclerViewAvailable.layoutManager= LinearLayoutManager(AvailableFragment.newInstance().context)
        viewHolder.view.recyclerViewAvailable.adapter = foods?.let { AdapterCard(it, repository) }

    }

    override fun getItemCount()=data.size

    fun updateList(availables: List<Category>) {
        data = availables
        notifyDataSetChanged()
    }

}