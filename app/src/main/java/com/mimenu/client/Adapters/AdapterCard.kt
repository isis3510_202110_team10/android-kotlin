package com.mimenu.client.Adapters

import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mimenu.client.Data.Repository.CategoryRepository
import com.mimenu.client.Model.Food
import com.mimenu.client.Model.RequestAvailable
import com.mimenu.client.R
import com.mimenu.client.Singletons.UserData
import com.mimenu.client.ViewHolders.Card_Available_ViewHorlder
import kotlinx.android.synthetic.main.available_cards.view.*
import javax.inject.Inject

class AdapterCard @Inject constructor(val data: List<Food>, val repository: CategoryRepository): RecyclerView.Adapter<Card_Available_ViewHorlder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): Card_Available_ViewHorlder {
        return Card_Available_ViewHorlder(LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.available_cards, viewGroup, false))
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(viewHolder: Card_Available_ViewHorlder, i: Int) {
        val item = data[i]
        viewHolder.view.item_Name.text = item.name
        viewHolder.view.item_detail.text= item.desc
        viewHolder.view.item_price.text= "$"+ item.price.toString()
        viewHolder.view.switch_available.isChecked= item.dis


        Glide.with(viewHolder.view.context)
                .load(item.img)
                .into(viewHolder.view.item_image)

        viewHolder.view.switch_available.setOnCheckedChangeListener { _, isChecked ->
            var operation = "remove"
            if(!isChecked)operation = "add"
            val request = RequestAvailable(null, UserData.idVenue,item.id,operation)
            repository.updateAvailable(request)
        }

    }


    override fun getItemCount()=data.size
}