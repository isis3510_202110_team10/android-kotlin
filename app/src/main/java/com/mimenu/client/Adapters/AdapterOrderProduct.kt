package com.mimenu.client.Adapters

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mimenu.client.Model.OrderProduct
import com.mimenu.client.R
import com.mimenu.client.ViewHolders.OrderProductViewHolder
import kotlinx.android.synthetic.main.order_product_cards.view.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import com.mimenu.client.Model.Choice
import com.mimenu.client.Model.Status

class AdapterOrderProduct (val orderProducts: List<OrderProduct>, private val listener: onItemClickListener,private val position: Int) : RecyclerView.Adapter<OrderProductViewHolder>() {

    override fun getItemCount() = orderProducts.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderProductViewHolder {
        return OrderProductViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.order_product_cards,parent,false),listener,position
        )
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: OrderProductViewHolder, position: Int) {
        val order = orderProducts[position]

        holder.view.textViewName.text = order.product_name
        holder.view.textViewDishes.text = order.quantity.toString() + if (  order.quantity >1 ) " platos" else " plato"
        holder.view.textViewTime.text = getDate(order.created_at)
        holder.view.textViewStatus.text = order.status_display
        holder.view.textViewOptions.text = formatOptions(order)
        setState(holder,order.status)
        Glide.with(holder.view.context)
        .load(order.image_url)
        .into(holder.view.imageView)
    }

    private fun formatOptions(order: OrderProduct):String{
        var rta = ""
        for(choice:Choice in order.choices) rta += choice.option_name + ": "+ choice.name + "\n"
        if (rta == "") return "Sin adiciones"
        return rta
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getDate(created_at:String?): String{
        var rta = ""
        var formater = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")
        var to_date = LocalDate.parse(created_at?.substring(0,19),formater)
        val diff_days = ChronoUnit.DAYS.between(to_date,LocalDate.now())
        if(diff_days== 0L) "Hoy a las " + created_at?.substring(11,16)
        else rta = "Hace "+diff_days+" días a las "+ created_at?.substring(11,16)
        return rta
    }

    private fun setState(holder: OrderProductViewHolder,status: Int?){
        if (status != null) {
            if (status >= Status.CACHED) {
                holder.view.buttonAccept.visibility = View.GONE
                holder.view.buttonReject.visibility = View.GONE
                holder.view.buttonCancel.visibility = View.GONE
                holder.view.buttonDeliver.visibility = View.GONE
                holder.view.textViewStatus.visibility = View.GONE
                holder.view.progressBar.visibility= View.VISIBLE
            }
            else if (status == Status.CREATED) {
                holder.view.buttonCancel.visibility = View.GONE
                holder.view.buttonDeliver.visibility = View.GONE
                holder.view.textViewStatus.visibility = View.GONE
            }
            else if (status == Status.ACCEPTED) {
                holder.view.buttonAccept.visibility = View.GONE
                holder.view.buttonReject.visibility = View.GONE
                holder.view.textViewStatus.visibility = View.GONE
            }
            else if (status > Status.ACCEPTED) {
                holder.view.buttonCancel.visibility = View.GONE
                holder.view.buttonDeliver.visibility = View.GONE
                holder.view.buttonAccept.visibility = View.GONE
                holder.view.buttonReject.visibility = View.GONE
            }
        }
    }

    interface onItemClickListener{
        fun onItemClick(position:Int,parentPosition:Int,description:String)
    }
}