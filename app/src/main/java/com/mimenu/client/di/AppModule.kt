package com.mimenu.client.di

import android.app.Application
import androidx.room.Room
import com.mimenu.client.API.AvailableAPI
import com.mimenu.client.Data.DataBase
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import com.mimenu.client.API.VenueAPI
import com.mimenu.client.API.OrderAPI

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit=
            Retrofit.Builder()
                    .baseUrl(AvailableAPI.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

    @Provides
    @Singleton
    fun providesAvailableApi(retrofit: Retrofit): AvailableAPI =
            retrofit.create(AvailableAPI:: class.java)

    @Provides
    @Singleton
    fun providesVenueApi(retrofit: Retrofit): VenueAPI =
            retrofit.create(VenueAPI:: class.java)

    @Provides
    @Singleton
    fun providesDataTable(app:Application): DataBase =
            Room.databaseBuilder(app, DataBase::class.java, "database")
                    .fallbackToDestructiveMigration()
                    .build()
    @Provides
    @Singleton
    fun providesOrderApi(retrofit: Retrofit): OrderAPI =
            retrofit.create(OrderAPI:: class.java)

}