package com.mimenu.client.API

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.*

interface OrderAPI {

    @GET("orders/?status=1,2")
    suspend fun getOrders(@HeaderMap headers: Map<String,String>): JsonArray

    @PATCH("orders/items/{id}/")
    fun updateOrder(@Header("Content-Type") content:String, @Header("Authorization") token:String, @Body body: Map<String,String>,@Path(value="id") id: String): Call<JsonObject>
}