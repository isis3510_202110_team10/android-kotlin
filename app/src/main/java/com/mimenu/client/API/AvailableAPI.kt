package com.mimenu.client.API

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.*

interface AvailableAPI {
    companion object{
        const val BASE_URL="https://api.compleat.com.co/api/"
    }
    

    @GET("menu/")
    suspend fun getAvailables(@HeaderMap headers: Map<String,String>):JsonArray


    @POST("menu/not_available_venue")
    fun updateAvailable(@Header("Content-Type") content:String, @Header("Authorization") token:String, @Body body: Map<String,String>): Call<JsonObject>

}