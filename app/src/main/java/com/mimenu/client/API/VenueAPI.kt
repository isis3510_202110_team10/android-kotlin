package com.mimenu.client.API

import com.google.gson.JsonArray
import retrofit2.http.GET
import retrofit2.http.HeaderMap

interface VenueAPI {

    @GET("restaurants/venues/")
    suspend fun getVenues(@HeaderMap headers: Map<String,String>): JsonArray

}