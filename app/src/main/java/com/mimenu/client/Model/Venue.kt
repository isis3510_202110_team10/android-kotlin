package com.mimenu.client.Model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Venue")
data class Venue(
        @ColumnInfo(name="id")
        @PrimaryKey
        var id : String,
        var name : String,
        var address: String,
        var latitude : Double?,
        var longitude : Double?,
        var created_at : String,
        var updated_at : String,
        var restaurant: String
)



{
    constructor() : this(" ", "","",0.0,0.0, "","","")
}