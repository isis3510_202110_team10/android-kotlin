package com.mimenu.client.Model

import androidx.room.*
import com.mimenu.client.Data.Converter.ConverterCategory

@Entity(tableName = "Category")
data class Category(
        @ColumnInfo(name="name")
        @PrimaryKey
        val name: String,

        @TypeConverters(value=[ConverterCategory::class])
        val foods: List<Food>?
){
        constructor() : this(" ", emptyList())
}