package com.mimenu.client.Model

import androidx.room.Entity

@Entity(tableName = "Choice")
data class Choice (
        val name: String,
        val option_name: String,
        val extra_cost : Double,
){
    constructor() : this(" ", "",0.0)
}