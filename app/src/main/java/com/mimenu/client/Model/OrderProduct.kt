package com.mimenu.client.Model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.mimenu.client.Data.Converter.ConverterChoice

@Entity(tableName = "OrderProduct")
data class OrderProduct (
        @ColumnInfo(name="id")
        @PrimaryKey
        val id : Long,
        @TypeConverters(value=[ConverterChoice::class])
        val choices : List<Choice>,
        val product_name : String,
        val quantity : Int,
        val comments : String,
        val price : String,
        val image_url : String,
        var status : Int,
        val status_display : String,
        val accepted_at : String?,
        val billed_at : String?,
        val cancelled_at : String?,
        val created_at : String?,
        val delivered_at : String?,
        val not_accepted_at : String?,
        val user_cancelled_at : String?
){
    constructor() : this(0, emptyList(),"",0,"","","",0,"","","","","","","","")
}