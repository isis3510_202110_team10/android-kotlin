package com.mimenu.client.Model

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "RequestOrder")
data class RequestOrder (
        @PrimaryKey(autoGenerate = true)
        val id: Int?,
        var item_id : String?,
        var status : Int?
)