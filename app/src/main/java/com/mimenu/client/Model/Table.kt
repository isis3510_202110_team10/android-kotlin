package com.mimenu.client.Model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Table")
data class Table (
        @ColumnInfo(name="id")
        @PrimaryKey
        var id:  Long,
        var name : String,
        var top : Int,
        var left : Int,
        var venue: String
){
    constructor():this(0,"",0,0,"")
}