package com.mimenu.client.Model

import androidx.room.*
import com.mimenu.client.Data.Converter.ConverterOrderProduct
import com.mimenu.client.Data.Converter.ConverterTable

@Entity(tableName = "Order")
data class Order(
        @TypeConverters(value=[ConverterOrderProduct::class])
        var items : List<OrderProduct>,
        @TypeConverters(value=[ConverterTable::class])
        var table : Table?,
        @ColumnInfo(name="id")
        @PrimaryKey
        var id : String,
        var venue : String?,
        var public_id : Long?,
        var created_at : String?,
        var status : Int?,
        var status_display : String?,
        var source : String?
){
        constructor() : this(emptyList(),Table(),"","",0,"",0,"","")
}