package com.mimenu.client.Model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "selectedVenue")
data class SelectedVenue (
    @PrimaryKey
    val id:Int?,
    val id_venue: String?
)