package com.mimenu.client.Model

object Status {
    val CREATED = 1
    val ACCEPTED = 2
    val DELIVERED = 3
    val BILLED = 4
    val USER_CANCELLED = 5
    val NOT_ACCEPTED = 6
    val CANCELLED_DURING_PREPARATION = 7
    val CACHED = 8
}