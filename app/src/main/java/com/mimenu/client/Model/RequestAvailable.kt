package com.mimenu.client.Model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "RequestAvailable")
data class RequestAvailable (
        @PrimaryKey(autoGenerate = true)
        val id: Int?,
        val id_venue: String?,
        val  item_id: String?,
        val operation: String?
)