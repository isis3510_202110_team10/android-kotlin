package com.mimenu.client.Model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Foods")
data class Food(

        @PrimaryKey
        val id: String,
        val name: String,
        val desc:String,
        val price: Int,
        var dis: Boolean,
        val img: String
){
        constructor():this("","","",0,false, "")
}
