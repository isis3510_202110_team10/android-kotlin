package com.mimenu.client.UI.fragments

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.*
import com.mimenu.client.Data.Repository.CategoryRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@RequiresApi(Build.VERSION_CODES.O)
@HiltViewModel
class AvailableViewModel @Inject constructor(
        repository: CategoryRepository): ViewModel(){

    val repository = repository
    val categories= repository.getAvailables().asLiveData()

    var syncInProgress = false

    fun sync() {
        CoroutineScope(Dispatchers.Main).launch {
        if (!syncInProgress) {
            syncInProgress = true
            repository.updateRequests()
            syncInProgress = false
        }
        }

    }

}