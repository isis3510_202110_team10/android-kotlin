package com.mimenu.client.UI

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.mimenu.client.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.menu_activity.*


@AndroidEntryPoint
class MenuActivity :AppCompatActivity(){

    private lateinit var navController:NavController

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.menu_activity)

        setSupportActionBar(toolbar)
        getSupportActionBar()!!.setTitle("Compleat")

        navController=Navigation.findNavController(this, R.id.fragment)

        NavigationUI.setupWithNavController(navigation_view,navController)
        NavigationUI.setupActionBarWithNavController(this, navController, menu)

    }


    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(
                navController, menu
        )
    }

}