package com.mimenu.client.UI

import com.mimenu.client.Helpers.ConnectivityHelper
import android.app.Dialog
import android.content.Intent
import android.content.IntentFilter
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.auth0.android.Auth0
import com.auth0.android.Auth0Exception
import com.auth0.android.authentication.AuthenticationException
import com.auth0.android.provider.AuthCallback
import com.auth0.android.provider.VoidCallback
import com.auth0.android.provider.WebAuthProvider
import com.auth0.android.result.Credentials
import com.mimenu.client.Helpers.ConnectionListener
import com.mimenu.client.R
import com.mimenu.client.Singletons.UserData
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_sede.*
import kotlinx.android.synthetic.main.activity_login.loginButton
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


/**
 * Created by Grupo 5
 */
class LoginActivity : AppCompatActivity() , ConnectionListener,View.OnClickListener {
    private var auth0: Auth0? = null
    private var loginEnabled : Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loginButton.setOnClickListener(this)

        ConnectivityHelper.changeListener(this)
        val intentFilter = IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION)
        registerReceiver(ConnectivityHelper, intentFilter)
        updateConnectionState(ConnectivityHelper.isConnected())

        auth0 = Auth0(this)
        auth0!!.isOIDCConformant = true

        //Check if the activity was launched to log the user out
        if (intent.getBooleanExtra(EXTRA_CLEAR_CREDENTIALS, false)) {
            logout()
        }
    }

    private fun login() {
        WebAuthProvider.login(auth0!!)
                .withScheme("demo")
                .withAudience(String.format("https://%s/userinfo", getString(R.string.com_auth0_domain)))
                .start(this, object : AuthCallback {
                    override fun onFailure(dialog: Dialog) {
                        runOnUiThread { dialog.show() }
                    }

                    override fun onFailure(exception: AuthenticationException) {}

                    override fun onSuccess(credentials: Credentials) {
                        runOnUiThread {
                            val intent = Intent(applicationContext, VenueActivity::class.java)
                            UserData.setAccessToken(credentials.accessToken.toString())
                            UserData.setIdToken(credentials.idToken.toString())
                            startActivity(intent)
                            finish()
                        }
                    }
                })
    }

    private fun logout() {
        WebAuthProvider.logout(auth0!!)
                .withScheme("demo")
                .start(this, object : VoidCallback {
                    override fun onSuccess(payload: Void?) {}
                    override fun onFailure(error: Auth0Exception) {
                        //Log out canceled, keep the user logged in
                        showNextActivity()
                    }
                })
    }

    private fun showNextActivity() {
        val intent = Intent(this@LoginActivity, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    companion object {
        const val EXTRA_CLEAR_CREDENTIALS = "com.auth0.CLEAR_CREDENTIALS"
        const val EXTRA_ACCESS_TOKEN = "com.auth0.ACCESS_TOKEN"
        const val ID_TOKEN = "com.auth0.ID_TOKEN"
    }

    override fun onConnectionStateChanged(state: Boolean) {
        updateConnectionState(state)
    }

    fun updateConnectionState(state:Boolean){
        CoroutineScope(Dispatchers.Main).launch {
            if (state){
                internet_status_login.visibility = View.GONE
                loginEnabled = true
                loginButton.setBackgroundTintList(getResources().getColorStateList(R.color.colorAccent))
            }
            else{
                internet_status_login.visibility = View.VISIBLE
                loginEnabled=false
                loginButton.setBackgroundTintList(getResources().getColorStateList(R.color.lightgray))
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    @Override
    override fun onClick(view :View?){
        var description = view?.resources!!.getResourceEntryName(view?.id!!)
        if(description.equals("loginButton")){
            if(loginEnabled) login()
        }
    }
}