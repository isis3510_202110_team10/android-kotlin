package com.mimenu.client.UI

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import com.mimenu.client.R

/**
 * Created by Grupo 5
 */
class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        val loginButton = findViewById<Button>(R.id.logout)
        loginButton.setOnClickListener { logout() }

        val siguienteButton = findViewById<Button>(R.id.siguiente)
        siguienteButton.setOnClickListener{siguiente()}
    }

    private fun logout() {
        val intent = Intent(this, LoginActivity::class.java)
        intent.putExtra(LoginActivity.EXTRA_CLEAR_CREDENTIALS, true)
        startActivity(intent)
        finish()
    }

    private fun siguiente(){
        val intent = Intent(this, MenuActivity::class.java)
        startActivity(intent)
        finish()
    }

}
