package com.mimenu.client.UI.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.mimenu.client.R
import com.mimenu.client.UI.LoginActivity
import com.mimenu.client.UI.VenueActivity
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings

@AndroidEntryPoint
@WithFragmentBindings
class SessionFragment: Fragment() {

    companion object {
        fun newInstance() = SessionFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.session_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val loginButton = activity?.findViewById<Button>(R.id.logout)
        loginButton?.setOnClickListener { logout() }

        val sedesButton = activity?.findViewById<Button>(R.id.sedes)
        sedesButton?.setOnClickListener{sedes()}
    }


    private fun logout() {

        val intent = Intent(activity, LoginActivity::class.java)
        intent.putExtra(LoginActivity.EXTRA_CLEAR_CREDENTIALS, true)
        startActivity(intent)
        activity?.finish()
    }

    private fun sedes(){
        val intent = Intent(activity, VenueActivity::class.java)
        startActivity(intent)
        activity?.finish()
    }


}