package com.mimenu.client.UI

import android.app.AlertDialog
import android.app.DialogFragment
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.mimenu.client.Adapters.AdapterVenue
import com.mimenu.client.Helpers.ConnectionListener
import com.mimenu.client.Helpers.ConnectivityHelper
import com.mimenu.client.Helpers.LocationHelper
import com.mimenu.client.Model.SelectedVenue
import com.mimenu.client.Model.Venue
import com.mimenu.client.R
import com.mimenu.client.Singletons.UserData
import com.mimenu.client.UI.fragments.GeoDialogFragment
import com.mimenu.client.UI.fragments.VenueViewModel
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.android.synthetic.main.activity_sede.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@AndroidEntryPoint
@WithFragmentBindings
class VenueActivity : AppCompatActivity(), AdapterVenue.onItemClickListener, ConnectionListener,View.OnClickListener {

    private lateinit var locationHelper : LocationHelper
    private val viewModel: VenueViewModel by viewModels()
    private var venues= arrayListOf<Venue>()

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sede)

        locationHelper = LocationHelper(this, applicationContext )

        locationHelper.initLocationListener()

        buttonGPSDenied.setOnClickListener(this)

        if(!locationHelper.permissionIsNotAccepted()){
            this.let { viewModel.venuesLiveData.observe(it){
                vens->
                venues = vens.data as ArrayList<Venue>
                recyclerViewVenue.layoutManager= LinearLayoutManager(this)
                recyclerViewVenue.adapter= AdapterVenue(venues,this)
            }
            }
        }

        ConnectivityHelper.changeListener(this)
        updateConnectionState(ConnectivityHelper.isConnected())
        preferedVenue()

    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onItemClick(position: Int) {
        var clickedItem : Venue = venues[position]
        checkPosition(clickedItem)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == locationHelper.locationPermissionCode) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "GPS Permission Granted", Toast.LENGTH_SHORT).show()
                locationHelper.getLocation()
                buttonGPSDenied.visibility = View.GONE
                textGPSDenied.visibility = View.GONE
                recyclerViewVenue.visibility = View.VISIBLE
                this.let { viewModel.venuesLiveData.observe(it){
                    vens->
                    venues = vens.data as ArrayList<Venue>
                    recyclerViewVenue.layoutManager= LinearLayoutManager(this)
                    recyclerViewVenue.adapter= AdapterVenue(venues,this)
                }
                }
            }
            else {
                buttonGPSDenied.visibility = View.VISIBLE
                textGPSDenied.visibility = View.VISIBLE
                recyclerViewVenue.visibility = View.GONE
            }
        }
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun preferedVenue(){
        CoroutineScope(Dispatchers.Main).launch {
            val pref = viewModel.getPreferedVenue()
            Log.i("preferred",pref.toString())
            if (pref != null) {
                dialog(pref)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun dialog(venue:Venue){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Sede preferida")
        builder.setMessage("¿Desea entrar a la sede:".plus(venue.name).plus("?"))

        builder.setPositiveButton("Aceptar", {
            dialog,id->checkPosition(venue)
        })

        builder.setNegativeButton("Cancelar", null)
        val dialog = builder.create()
        dialog.show()

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun redirect(venue:String) {
        val ven = SelectedVenue(null, venue)
        viewModel.insertSelectedVenue(ven)
        UserData.setIdVenue(venue)
        val intent = Intent(this, MenuActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onConnectionStateChanged(state: Boolean) {
        updateConnectionState(state)
    }

    fun updateConnectionState(state:Boolean){
        CoroutineScope(Dispatchers.Main).launch {
            if (state) internet_status_venue.visibility = View.GONE
            else internet_status_venue.visibility = View.VISIBLE
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun checkPosition(clickedItem:Venue){
        var msg = locationHelper.getResponse(clickedItem.latitude, clickedItem.longitude)
        var bund = Bundle()
        bund.putString("msg",msg)

        //msg = "Pasa"
        if(msg.equals("Pasa")){
            redirect(clickedItem.id)
        }
        else{
            val geoFragment: DialogFragment = GeoDialogFragment().apply{arguments = bund}
            geoFragment.show(this!!.fragmentManager,"name")
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    @Override
    override fun onClick(view :View?){
        var description = view?.resources!!.getResourceEntryName(view?.id!!)
        if(description.equals("buttonGPSDenied")){
            locationHelper.initLocationListener()
        }
    }

}