package com.mimenu.client.UI.fragments

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.mimenu.client.Adapters.AdapterCategories
import com.mimenu.client.Adapters.AdapterOrders
import com.mimenu.client.Helpers.ConnectionListener
import com.mimenu.client.Helpers.ConnectivityHelper
import com.mimenu.client.Model.Category
import com.mimenu.client.Model.Order
import com.mimenu.client.R
import com.mimenu.client.Services.Resource
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.android.synthetic.main.available_fragment.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@AndroidEntryPoint
@WithFragmentBindings
class AvailableFragment : Fragment(),ConnectionListener {


    private val viewModel: AvailableViewModel by viewModels()
    private var availables : List<Category> = listOf()
    private lateinit var adapter: AdapterCategories

    companion object {
        fun newInstance() = AvailableFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.available_fragment, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter= AdapterCategories(availables, viewModel.repository)
        recyclerViewCategories.layoutManager= LinearLayoutManager(activity)
        recyclerViewCategories.adapter = adapter

        progress_bar_connection.isVisible=false
        text_connection.isVisible=false
        text_connection.text=" "

        activity?.let { viewModel.categories.observe(it){
            result->
            availables = result.data!!
            adapter.updateList(availables)

            progress_bar_connection.isVisible=result is Resource.Loading<*> && result.data.isNullOrEmpty()
            text_connection.isVisible=result is Resource.Error<*> && result.data.isNullOrEmpty()
            text_connection.text="No hay conexión a internet, por favor acceda a una red wifi o prenda los datos móviles"
        }
        }
        ConnectivityHelper.changeListener(this)
        updateConnectionState(ConnectivityHelper.isConnected())
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onConnectionStateChanged(state:Boolean){
        updateConnectionState(state)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun updateConnectionState(state:Boolean){
        CoroutineScope(Dispatchers.Main).launch {
            if (state) internet_status_available.visibility = View.GONE
            else internet_status_available.visibility = View.VISIBLE
        }
        if (state) viewModel.sync()
    }

}
