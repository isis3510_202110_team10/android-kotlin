package com.mimenu.client.UI.fragments
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.*
import com.mimenu.client.Data.Repository.OrderRepository
import com.mimenu.client.Model.Order
import com.mimenu.client.Services.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import javax.inject.Inject

@RequiresApi(Build.VERSION_CODES.O)
@HiltViewModel
class OrderViewModel @Inject constructor(
        repository: OrderRepository): ViewModel() {

    val repository = repository
    var ordersList = repository.getOrders().asLiveData()

    var syncInProgress = false

   suspend fun update(): List<Resource<List<Order>>> {
        return repository.getOrders().toList()
    }

    fun sync() {
        CoroutineScope(Dispatchers.Main).launch {
            if (!syncInProgress) {
                syncInProgress = true
                repository.updateRequests()
                syncInProgress = false
            }
        }
    }


}