package com.mimenu.client.UI.fragments

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.*
import com.mimenu.client.Model.Venue
import com.mimenu.client.API.VenueAPI
import com.mimenu.client.Data.Repository.SelectedVenueRepository
import com.mimenu.client.Data.VenueRepository
import com.mimenu.client.Model.SelectedVenue
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@RequiresApi(Build.VERSION_CODES.O)
@HiltViewModel
class VenueViewModel @Inject constructor(
        api: VenueAPI, repositorySelected: SelectedVenueRepository, repositoryVenue: VenueRepository): ViewModel(){

    private val repositorySelected=repositorySelected
    private val repositoryVenue=repositoryVenue
    val venuesLiveData= repositoryVenue.getVenues().asLiveData()


    fun insertSelectedVenue(venue: SelectedVenue){
        repositorySelected.insertVenue(venue)
    }

    suspend fun getPreferedVenue(): Venue? {
        val id=repositorySelected.getPreferedVenue()
        if(id!=null || !id.equals(" ")){
            return repositoryVenue.getVenue(id)
        }
        else{ return null}
    }
}