package com.mimenu.client.UI.fragments

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.annotation.RequiresApi
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.mimenu.client.Adapters.AdapterOrderProduct
import com.mimenu.client.Adapters.AdapterOrders
import com.mimenu.client.Helpers.ConnectionListener
import com.mimenu.client.Helpers.ConnectivityHelper
import com.mimenu.client.Model.Order
import com.mimenu.client.Model.RequestOrder
import com.mimenu.client.Model.Status
import com.mimenu.client.R
import com.mimenu.client.Services.Resource
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.android.synthetic.main.order_fragment.*
import kotlinx.coroutines.*

@AndroidEntryPoint
@WithFragmentBindings
class OrderFragment : Fragment(), AdapterOrderProduct.onItemClickListener, ConnectionListener {

    private val viewModel: OrderViewModel by viewModels()
    private var orders : List<Order> = listOf()
    private lateinit var adapter: AdapterOrders

    companion object {
        fun newInstance() = OrderFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.order_fragment, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        addSpinnersToUI()
        ConnectivityHelper.changeListener(this)
        updateConnectionState(ConnectivityHelper.isConnected())
        recyclerViewOrders.layoutManager = LinearLayoutManager(activity)
        adapter=AdapterOrders( orders,this)
        recyclerViewOrders.adapter = adapter

        activity?.let{
            viewModel.ordersList.observe(it){
                ordersList ->
                orders = ordersList.data!!
                adapter.updateList(orders)
                startUpdates()
            }

        }


    }


    fun addSpinnersToUI() {
        val spinners = listOf(R.id.status_spinner,R.array.status_array,R.id.table_spinner,R.array.table_array)
        for (i in 0..(spinners.size-1) step 2){
            var spinner : Spinner = requireActivity().findViewById(spinners[i])
            ArrayAdapter.createFromResource(activity,
                    spinners[i+1],
                    android.R.layout.simple_spinner_item).also{adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinner.adapter = adapter
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onItemClick(position:Int, parentPosition:Int, description:String) {
        val act = getActivity()
        var order = orders[parentPosition].items[position]
        var bund = Bundle()
        var msg = setMessage(order.product_name,description,order.id.toString(),parentPosition,position)
        bund.putString("message",msg)

        val orderDialog : OrderDialogFragment = OrderDialogFragment().apply{arguments=bund}
        orderDialog.show(act!!.fragmentManager,"name")
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun setMessage(name:String, desc: String, item_id:String,parentPosition:Int, position:Int): String{
        var status = 0
        var rta =""
        if(desc.equals("buttonAccept")){
            rta = name+ " en preparación !"
            status = Status.ACCEPTED
        }
        else if(desc.equals("buttonReject")){
            rta = name+ " rechazado !"
            status = Status.NOT_ACCEPTED
        }
        else if(desc.equals("buttonCancel")){
            rta = name+" cancelado !"
            status = Status.CANCELLED_DURING_PREPARATION
        }
        else{
            rta = name+" entregado !"
            status = Status.DELIVERED
        }

        orders[parentPosition].items[position].status = (status.toString() + Status.CACHED.toString()).toInt()
        val request = RequestOrder(null, item_id,status)
        viewModel.repository.updateOrder(request,orders[parentPosition])
        recyclerViewOrders.adapter!!.notifyItemChanged(parentPosition)
        return rta
    }
    val scope = MainScope() // could also use an other scope such as viewModelScope if available
    var job: Job? = null


    @RequiresApi(Build.VERSION_CODES.O)
    fun startUpdates() {
        stopUpdates()
        job = scope.launch {
            while(true) {
                if(ConnectivityHelper.isConnected()) {
                    activity?.let{
                        viewModel.update()
                        viewModel.ordersList.observe(it){
                            ordersList ->
                            orders = ordersList.data!!
                            adapter.updateList(orders)
                        }
                    }
                    }

                delay(15000)
            }
        }
    }

    fun stopUpdates() {
        job?.cancel()
        job = null
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onConnectionStateChanged(state: Boolean) {
        updateConnectionState(state)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun updateConnectionState(state:Boolean){
        CoroutineScope(Dispatchers.Main).launch {
            if (state) internet_status_order.visibility = View.GONE
            else internet_status_order.visibility = View.VISIBLE
        }
        if (state) viewModel.sync()
    }



}