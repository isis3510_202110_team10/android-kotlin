package com.mimenu.client.UI.fragments

import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle
import androidx.appcompat.app.AlertDialog


class GeoDialogFragment  : DialogFragment(){

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        return activity?.let {

            var msg = ""

            arguments?.let{
                msg= it.getString("msg")
            }

            val builder = AlertDialog.Builder(it)
            builder.setMessage(msg)
                    .setPositiveButton("Accept",
                            { dialog, id ->
                                // FIRE ZE MISSILES!
                            })
                    .setNegativeButton("Cancel",
                            { dialog, id ->
                                // User cancelled the dialog
                            })
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

}