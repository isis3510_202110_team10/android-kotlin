package com.mimenu.client.Helpers

interface ConnectionListener {

    fun onConnectionStateChanged(state:Boolean)
}