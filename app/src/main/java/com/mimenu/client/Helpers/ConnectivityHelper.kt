package com.mimenu.client.Helpers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkRequest
import android.os.Build
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.mimenu.client.R

object ConnectivityHelper : BroadcastReceiver() {

    private var connectionState = false
    private lateinit var listener : ConnectionListener

    override fun onReceive(context: Context, intent : Intent){
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val builder: NetworkRequest.Builder = NetworkRequest.Builder()
        connectivityManager.registerNetworkCallback(
                builder.build(),
                object : ConnectivityManager.NetworkCallback() {
                    @RequiresApi(Build.VERSION_CODES.O)
                    override fun onAvailable(network: android.net.Network?) {
                       if(!connectionState){
                           Toast.makeText(context, R.string.internet_connected, Toast.LENGTH_SHORT).show()
                           listener.onConnectionStateChanged(true)
                       }
                        connectionState = true
                    }
                    override fun onLost(network: android.net.Network?) {
                        connectionState = false
                        listener.onConnectionStateChanged(connectionState)
                    }
                }
        )
    }

    fun isConnected():Boolean{
        return connectionState
    }

    fun changeListener(listen:ConnectionListener){
        listener=listen
    }

}