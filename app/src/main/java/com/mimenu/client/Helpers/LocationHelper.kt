package com.mimenu.client.Helpers

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import com.mimenu.client.Singletons.UserData

class LocationHelper(val activity: Activity, val context : Context) : LocationListener {

    private lateinit var locationManager: LocationManager
    val locationPermissionCode = 1

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
        TODO("Not yet implemented")
    }

    override fun onProviderEnabled(p0: String?) {
        TODO("Not yet implemented")
    }

    override fun onProviderDisabled(p0: String?) {
        TODO("Not yet implemented")
    }

    fun getResponse(lat: Double?,lon: Double?) : String{
        if(lat != null && lon != null) {
            var dis = getDistance(lat, lon, UserData.latitude, UserData.longitude)

            if (dis > 10) {
                return "No se encuentra cerca del restaurante. Por favor mantenerse a un radio de 10 mts."
            } else {
                return "Pasa"
            }
        }
        return "La posición de esta sede no está registrada"
    }

    @SuppressLint("MissingPermission")
    fun getLocation() {
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5f, this)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun initLocationListener(){
        locationManager = activity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if ((ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            activity.requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), locationPermissionCode)
        }
        else{
            getLocation()
        }
    }

    fun permissionIsNotAccepted(): Boolean{
        return ((ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED))
    }

    private fun getDistance(lat1 : Double, lon1 : Double, lat2 : Double, lon2 : Double): Double {
        val latr1 = Math.toRadians(lat1)
        val latr2 = Math.toRadians(lat2)
        val lonr1 = Math.toRadians(lon1)
        val lonr2 = Math.toRadians(lon2)

        val dlon = lonr2 - lonr1
        val dlat = latr2 - latr1
        val a = Math.pow(Math.sin(dlat / 2), 2.0) + Math.cos(latr1) * Math.cos(latr2) * Math.pow(Math.sin(dlon / 2), 2.0)
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))

        //Earth's mean radius in meters
        val r = 6378137

        return c*r
    }

    override fun onLocationChanged(location: Location) {
        UserData.setLatitude(location.latitude)
        UserData.setLongitude(location.longitude)
    }

}